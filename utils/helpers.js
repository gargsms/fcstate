'use strict';

import get from 'lodash/get';
import is from './is';

/**
 * Determines if the parent handler is to be invoked for a client change
 * @private
 * @param  {String}  parent Shallower path in the object
 * @param  {String}  child  Deeper path in the object
 * @return {Boolean}
 */
function _isSubkey (parent, child) {
  if (parent === '') {
    return true;
  }

  let parentHeirarchy = parent.split('.');
  let childHeirarchy = child.split('.');

  return parentHeirarchy.every((p, i) => {
    return p === childHeirarchy[i];
  });
}

/**
 * Invokes all the listeners there are for a set of paths
 * @memberOf helpers
 * @param  {Object} listeners The map of all listeners
 * @param  {String} path      The path to match for the listeners
 * @param  {Any}    oldValue  Previous value for the whole state instance
 * @param  {Any}    newValue  New value for the whole state instance
 * @return {null}
 */
function applyListeners (listeners, path, oldValue, newValue) {
  let listenerKeys = Object.keys(listeners);
  let listenerKeysToExecute = [];

  listenerKeys.forEach(k => {
    if (k === path || _isSubkey(k, path)) {
      listenerKeysToExecute.push(k);
    }
  });

  listenerKeysToExecute.forEach(k => {
    listeners[k].forEach(l => {
      l(get(oldValue, k), get(newValue, k));
    });
  });
}

/**
 * A better version of Object.assign that does a deep copy and not a shallow one
 * @memberOf helpers
 * @param  {Object} destination The object to copy over on
 * @param  {Object} source      The object to derive values from
 * @return {Object}
 */
function deepAssign (destination = {}, source = {}) {
  for (let key in source) {
    if (is.object(source[key])) {
      destination[key] = deepAssign(destination[key], source[key]);
    } else {
      destination[key] = source[key];
    }
  }
  return destination;
}

/**
 * @exports helpers
 */
export default {applyListeners, deepAssign};
