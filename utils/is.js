'use strict';

function defined (value) {
  return value !== void 0 && value != null;
}

function func (value) {
  return typeof value === 'function';
}

function object (value) {
  return typeof value === 'object';
}

function plainObject (value) {
  return object(value) && Object.prototype.toString.call(value) === '[object Object]';
}

function string (value) {
  return typeof value === 'string' && value.length > 0;
}

function undef (value) {
  return value === void 0;
}

/**
 * @exports is
 */
export default {defined, func, object, plainObject, string, undef};
