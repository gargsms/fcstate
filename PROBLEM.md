## FT Engg Assignment

### Problem Statement

Deliver JS library (State) which is useful in converting JS objects into observables and has handy API to manipulate the state.

#### Core deliverables:

    Code of the library (language: JavaScript, version: ES6)
    Build system which generates ES5 and ES6 code
    Unit tests with minimum 50% coverage
    Lint
    Documentation
    Sample

#### Methods

##### static create

Takes a simple JavaScript Object and make it observable. This returns the new object as a state instance which can be used to observe property change.

```javascript
let myState = State.create({ 
  range: {
    start: 1,
    end: 5 
  },
  visible: true 
});
```

##### getState

Since State.create does not return the JavaScript object back as it deserialise the JavaScript object to native data-structure, getState function comes to the rescue to get the JavaScript object back. The function returns the plain old JavaScript object.

```javascript
myState.getState();
// Output
{
  range: {
    start: 1,
    end: 5 
  },
  visible: true 
}
```

##### create

Appends property in the existing state (mutates the original state). The is called using two parameters. The first parameter is where to append the state and the second being what property to append.

```javascript
myState.create('range.type',  {
  absolute: true
});
```

If you do myState.getState() the following output is shown

```javascript
// Output
{
  range: {
    start: 1, 
    end: 5, 
    type: {
      absolute: true 
    }
  },
  visible: true 
}
```

The same function can be called with only one parameter, just by passing the new state properties. In this case it appends the property in the base.

```javascript
myState.create({ focus: null });
```

If you do myState.getState() the following output is shown

```javascript
// Output
{
  range: {
    start: 1, 
    end: 5, 
    type: {
      absolute: true 
    }
  },
  visible: true, 
  focus: null
}
```

This function returns the same state on which the method was called.

##### prop

This acts as getter and setter. If the function is called by passing only one argument, it retrieve the value associated with the property

```javascript
myState.prop('range.type')
// Output
{
  absolute: true
}
```

If the same function is called using two parameters, first one being the property and second one being the value, then the value is set for the property and the handlers are called(if any) which got registered using the on function

```javascript
myState.prop('visible', true)
```

This returns the instance on which it was called for chaining.

##### on 

This function takes a single property and handler which is called when any of the properties are changed. When a single property is changed the handler is called with two parameter, what was the old value of the state property and what is the new value.

```javascript
myState.on('range.start', (oldValue, newValue) => { 
  console.log('Value before prop change', oldValue);
  console.log('Value after prop change', newValue);
});

myState.prop('range.start', 9); 

// Output
Value before prop change 1 
Value after prop change 9
```

If a handler is registered on change of a property which has another state property as value, then the handler gets called whenever any state property connected to it gets changed

```javascript
myState.on('range', (oldValue, newValue) => { 
  console.log('Value before prop change', oldValue); 
  console.log('Value after prop change', newValue);
});

myState.prop('range.start', 10);

// Output
Value before prop change range {
  start: 9, 
  end: 5, 
  type: {
    absolute: true 
  }
}
Value after prop change range: {
  start: 10, 
  end: 5, 
  type: {
    absolute: true 
  }
}

myState.prop('range.type.absolute', false);

//Output
// Output
Value before prop change range {
  start: 10, 
  end: 5, 
  type: {
    absolute: true 
  }
}
Value after prop change range: {
  start: 10, 
  end: 5, 
  type: {
    absolute: false 
  }
}
```

The on returns a function which is when called the listener registered gets unregistered

```javascript
let unsub = myState.on('range.start', (oldVal, newVal) => { 
  console.log('Value before prop change', oldValue); 
  console.log('Value after prop change', newValue);
});

// Unsubscribe
unsub()
```


##### next

Just like the way on works, it just calls the handlers at the start of next event loop (next frame call) with all updates happened in the current frame in single go. The function definition and output schema remains same as on

```javascript
myState.next('range', (oldValue, newValue) => { 
  console.log('Value before prop change', oldValue); 
  console.log('Value after prop change', newValue);
});

myState.prop('range.start', 11);
myState.prop('range.end', 12);

// Output
Value before prop change range {
  start: 10, 
  end: 5, 
  type: {
    absolute: true 
  }
}
Value after prop change range: {
  start: 11, 
  end: 12, 
  type: {
    absolute: true
  }
}
```

##### lock and unlock

This helps control the call of handler when a property is changed.

```javascript
myState.on('range', (oldVal, newVal) => { 
  console.log('Value before prop change', oldValue); 
  console.log('Value after prop change', newValue);
 });

myState.prop('range.start', 12); 
// Output
Value before prop change {
  start: 11, 
  end: 12, 
  type: {
    absolute: true 
  }
}
Value after prop change {
  start: 12, 
  end: 12, 
  type: {
    absolute: true 
  }
}

myState.prop('range.end', 13); 
// Output
Value before prop change {
  start: 12, 
  end: 12, 
  type: {
    absolute: true 
  }
}
Value after prop change {
  start: 12, 
  end: 13, 
  type: {
    absolute: true 
  }
}
```

Here the handler is called twice, because two times the property was changed. This is not always desirable. For updating group of same property the user might want to the handler to get executed only once. This time locking and unlocking comes in picture

```javascript
myState.on('range', (oldVal, newVal) => { 
  console.log('Value before prop change', oldValue); 
  console.log('Value after prop change', newValue);
});

myState 
  .lock()
  .prop('range.start', 13) 
  .prop('range.end', 14) 
  .unlock()

// Output 
Value before prop change {
  start: 12, 
  end: 13, 
  type: {
    absolute: true 
  }
}
Value after prop change {
  start: 13, 
  end: 14, 
  type: {
    absolute: true 
  }
}
```

Once lock() is called the state caches all the change that comes after this. When unlock() is called it applies all the changes to the state and the handler is called.
