/* globals describe, it */

const assert = require('assert');

const State = require('../build/index-node').default;

describe('State', () => {
  describe('should not initialiaze', () => {
    it('as a function', () => {
      assert.throws(() => {
        State();
      }, /Cannot call a class as a function/);
    });

    it('without any passed argument', () => {
      assert.throws(State.create, /initialization/);
    });

    it('with an invalid argument', () => {
      assert.throws(() => {
        State.create('some string');
      }, /initialization/);
    });
  });

  it('should initialiaze with correct arguments', () => {
    assert.doesNotThrow(() => {
      State.create({val: true});
    });
  });

  it('should be able to return initialiazed state', () => {
    let originObject = {
      key: 'value'
    };
    let myState = State.create(originObject);

    assert.deepStrictEqual(originObject, myState.getState());
  });

  describe('should be able to', () => {
    describe('create new state entries', () => {
      it('on the root object', () => {
        let originObject = {
          key: 'value'
        };
        let myState = State.create(originObject);

        assert.deepStrictEqual(
          myState.create({
            anotherKey: 'withSomeValue'
          })
          .getState(), Object.assign({}, originObject, {
            anotherKey: 'withSomeValue'
          }));
      });

      it('on nested paths', () => {
        let originObject = {};
        let myState = State.create(originObject);

        assert.deepStrictEqual(
          myState.create('key[0].nestedKey', 'value')
            .getState(), Object.assign({}, originObject, {
              key: [{
                nestedKey: 'value'
              }]
            }));
      });
    });

    it('refuse creating bad keys on the root', () => {
      let myState = State.create({});

      assert.throws(() => {
        myState.create(undefined);
      }, /value to be an object/);

      assert.throws(() => {
        myState.create(null);
      }, /value to be an object/);
    });

    it('get property values', () => {
      let myState = State.create({
        key: 'value',
        anotherKey: {
          nested: 'some other value'
        }
      });

      assert.strictEqual(myState.prop('key'), 'value');

      assert.strictEqual(myState.prop('anotherKey.nested'), 'some other value');
    });

    describe('use prop', () => {
      it('to enable chaining', () => {
        let myState = State.create({
          key: null
        });

        assert.strictEqual(myState.prop('key', 'value') instanceof State, true);

        assert.deepStrictEqual(myState.getState(), {
          key: 'value'
        });
      });

      it('to update properties', () => {
        let myState = State.create({
          key: 'value'
        });

        assert.deepStrictEqual(myState.prop('key', 'updated value')
          .getState(), {
            key: 'updated value'
          });
      });

      it('but not to add properties', () => {
        let myState = State.create({});

        assert.throws(() => {
          myState.prop('key', 'value');
        }, /prop must only be used to retrieve, update properties/);
      });
    });
  });

  describe('should be able to', () => {
    it('refuse attaching bad listeners', () => {
      let myState = State.create({});

      assert.throws(() => {
        myState.on(null, null);
      }, /mutation listener/);

      assert.throws(() => {
        myState.on('', null);
      }, /mutation listener/);

      assert.throws(() => {
        myState.on('', () => {});
      }, /mutation listener/);
    });

    it('attach mutation listeners', () => {
      let myState = State.create({
        key: 'value'
      });

      assert.doesNotThrow(() => {
        myState.on('key', () => {});
      });
    });

    it('return unsubscribers', () => {
      let myState = State.create({
        key: 'value'
      });

      let unsubscriber = myState.on('key', () => {});

      assert.equal(typeof unsubscriber, 'function');
    });

    it('invoke listeners on property change with expected values', () => {
      let myState = State.create({
        key: 'value'
      });

      myState.on('key', (oldValue, newValue) => {
        assert.equal(oldValue, 'value');

        assert.equal(newValue, 'updated value');
      });

      myState.prop('key', 'updated value');
    });

    it('invoke listeners on parent properties for updates to children', () => {
      let myState = State.create({
        key: {
          nestedKey: 'value'
        }
      });

      myState.on('key', (oldValue, newValue) => {
        assert.deepStrictEqual(oldValue, {
          nestedKey: 'value'
        });

        assert.deepStrictEqual(newValue, {
          nestedKey: 'updated value'
        });
      });

      myState.on('key.nestedKey', (oldValue, newValue) => {
        assert.equal(oldValue, 'value');

        assert.equal(newValue, 'updated value');
      });

      myState.prop('key.nestedKey', 'updated value');
    });

    it('revoke listeners once used', () => {
      let myState = State.create({
        key: 'value'
      });
      let counter = 0;
      let unsubscriber = myState.on('key', (oldValue, newValue) => {
        assert.equal(oldValue, 'value');

        assert.equal(newValue, 'updated value');

        counter++;
      });

      myState.prop('key', 'updated value');

      unsubscriber();

      myState.prop('key', 'changing even more');

      assert.strictEqual(counter, 1);
    });
  });

  describe('should be able to', () => {
    it('lock, unlock the listeners', () => {
      let myState = State.create({
        key: 'value'
      });

      myState.on('key', (oldValue, newValue) => {
        assert.equal(oldValue, 'value');

        assert.equal(newValue, 'final value');
      });

      myState.lock()
        .prop('key', 'updated value')
        .prop('key', 'intermediate value')
        .prop('key', 'final value')
        .unlock();
    });

    describe('throw', () => {
      it('on locking an already locked state', () => {
        let myState = State.create({
          key: 'value'
        });

        assert.throws(() => {
          myState.lock()
            .lock();
        }, /Attempting to set lock/);
      });

      it('on unlocking an akready unlocked state', () => {
        let myState = State.create({});

        assert.throws(() => {
          myState.unlock();
        }, /Attempting to unlock/);
      });
    });
  });
});
