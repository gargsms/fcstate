let webpack = require('webpack');

module.exports = {
  entry: {
    js: './src/index.js'
  },
  output: {
    filename: './build/index-web.js',
    library: 'State'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {
        presets: ['es2015', 'env']
      }
    }]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin()
  ]
};
