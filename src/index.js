'use strict';

import get from 'lodash/get';
import is from '../utils/is';
import helpers from '../utils/helpers';
import set from 'lodash/set';

/**
 * The container for all our values of all State instances
 * @type {WeakMap}
 */
const POJO = new WeakMap();
/**
 * The listeners to be invoked on change in State properties
 * @type {WeakMap}
 */
const MUTATION_LISTENERS = new WeakMap();
/**
 * Lock holder for bulk consumption of changes
 * @type {WeakMap}
 */
const LOCK = new WeakMap();

/**
 * Factory Class for State instances
 * @module State
 * @exports State
 * @hideconstructor
 */
export default class State {
  constructor (obj) {
    POJO.set(this, obj);
    MUTATION_LISTENERS.set(this, {});
    LOCK.set(this, null);
  }

  /**
   * Class method to create a new instance
   *
   * @example
   * let myState = State.create({});
   *
   * @throws {Error} If the provided value is not correct
   *
   * @static
   * @param  {object} obj The value to initialize with
   * @return {State}      The State instance
   */
  static create (obj) {
    if (!is.plainObject(obj)) {
      throw new Error('Expected initialization with an object, was provided: ', typeof obj);
    }
    return new this(obj);
  }

  /**
   * Returns the current observable state of the instance
   * @return {object}
   */
  getState () {
    return POJO.get(this);
  }

  /**
   * Create a new key on the instance
   *
   * @throws {Error} If the provided path is not correct
   *
   * @param  {string|object}      path              The object path to append value at
   * @param  {object|undefined}   value             The value to be assigned
   * @param  {Boolean}            executeListeners  Determines if the listeners are to be called
   * @return {State}                                The current instance
   */
  create (path, value, executeListeners = false) {
    let shouldAssign = false;

    if (!is.defined(value) && !is.string(path)) {
      if (!is.plainObject(path)) {
        throw new Error('Expected value to be an object, was provided: ', typeof path);
      }
      shouldAssign = true;
    }

    let oldValue = this.getState();
    /*
     * This is necessary because Object.assign only does a shallow copy
     */
    let newValue = helpers.deepAssign({}, oldValue);

    if (shouldAssign) {
      Object.assign(newValue, path);
    } else {
      set(newValue, path, value);
    }

    if (executeListeners && LOCK.get(this) == null) {
      let listeners = MUTATION_LISTENERS.get(this);
      helpers.applyListeners(listeners, path, oldValue, newValue);
    }

    POJO.set(this, newValue);

    return this;
  }

  /**
   * Get or update an existing path with a new value
   *
   * @throws {Error} If the property is not set on the state
   *
   * @param  {string}           path  The object path to look at
   * @param  {undefined|Object} value New value to replace the old one with
   * @return {State|any}
   */
  prop (path, value) {
    if (is.undef(get(this.getState(), path))) {
      throw new Error('prop must only be used to retrieve, update properties');
    }

    if (is.defined(value)) {
      return this.create(path, value, true);
    } else {
      return get(this.getState(), path);
    }
  }

  /**
   * Attaches mutation listeners to the object paths
   *
   * @throws {Error} If wrong type of arguments are supplied
   *
   * @param  {string}   path     The path to watch for changes
   * @param  {Function} callback The function to be invoked on changes
   * @return {Function}          Unsubsriber function - removes the listener if called
   */
  on (path, callback) {
    if (!is.string(path) || !is.func(callback)) {
      throw new Error('Cannot attach mutation listener. Bad path or callback');
    }

    let listeners = MUTATION_LISTENERS.get(this);
    listeners[path] = listeners[path] || [];
    let length = listeners[path].push(callback);

    return () => {
      listeners[path].splice(length - 1, 1);
    };
  }

  /**
   * This was left unimplemented
   */
  next (path, callback) {
    // unimplemented
  }

  /**
   * Sets a lock on all listener invokations till the lock is removed
   *
   * @throws {Error} If it is being called agaim
   *
   * @return {State} The current instance for chaining purposes
   */
  lock () {
    if (LOCK.get(this) != null) {
      throw new Error('Attempting to set lock on already locked state');
    }

    LOCK.set(this, this.getState());
    return this;
  }

  /**
   * Removes the lock and invokes all the listeners at once
   *
   * @throws {Error} If it is being called again
   *
   * @return {null}
   */
  unlock () {
    if (LOCK.get(this) == null) {
      throw new Error('Attempting to unlock an already unlocked state');
    }

    let listeners = MUTATION_LISTENERS.get(this);
    helpers.applyListeners(listeners, '', helpers.deepAssign({}, LOCK.get(this)), this.getState());
    LOCK.set(this, null);
  }
}
