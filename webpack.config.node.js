module.exports = {
  entry: {
    js: './src/index.js'
  },
  output: {
    filename: './build/index-node.js',
    libraryTarget: 'commonjs'
  },
  target: 'node',
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {
        presets: ['env']
      }
    }]
  }
};
